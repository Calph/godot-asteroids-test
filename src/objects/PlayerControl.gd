extends KinematicBody2D

export (int) var forwardThrust = 40
export (int) var backwardThrust = 15
export (int) var maxSpeed = 500
export (float) var drag = 0.99

var velocity = Vector2(0,0)

func update_helpers():
	$DebugNode.update()
	get_node("/root/Master/Label").update()

# warning-ignore:unused_argument
func _process(delta):
	update_helpers()
	
func _input(event):
	if event is InputEventMouseMotion:
		rotation = \
			get_global_mouse_position() \
			.angle_to_point(position)
		update_helpers()

func get_input():
	var forward = Input.is_action_pressed("control_up")
	var backward = Input.is_action_pressed("control_down")
	
	if forward:
		velocity += Vector2(forwardThrust, 0).rotated(rotation)
		update_helpers()
	if backward:
		velocity -= Vector2(backwardThrust, 0).rotated(rotation)
		update_helpers()
	if velocity.length() > maxSpeed:
		velocity = velocity.normalized() * maxSpeed

# warning-ignore:unused_argument
func _physics_process(delta):
	get_input ()
	velocity = move_and_slide(velocity)
	velocity *= drag

	if position.x > get_viewport().size.x:
		position.x = 0
	if position.x < 0:
		position.x = get_viewport().size.x
	if position.y > get_viewport().size.y:
		position.y = 0
	if position.y < 0:
		position.y = get_viewport().size.y
