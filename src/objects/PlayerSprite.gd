extends Node2D

var font

func _ready():
	var label = Label.new()
	font = label.get_font("")
	#font.size = 12.0
	label.free()

func _draw():
	rotation = -$"../../Player".rotation
	draw_general_info()

func draw_general_info():
	var pV = $"../../Player".velocity
	var pR = $"../../Player".rotation
	var mA = get_local_mouse_position().angle_to_point(position)
		
	# Position Info
	draw_circle_arc(position, 48.0, 0, 360, Color.aquamarine * 0.6) # Outer
	draw_circle_arc(position, 32.0, 0, 360, Color.aquamarine) # Inner
	draw_circle(position, 4.0, Color.aquamarine)
	
	# Angle Display
	draw_line(position, Vector2(200,0), Color.black)
	draw_circle_arc(position, 32.0, 90,  rad2deg(mA) + 90 if mA < 0 else  rad2deg(mA-2*PI) + 90, Color.black)
	draw_string(font, position + Vector2(100,-4), "a: " + String(mA), Color.black)
	
	# Velocity Display
	draw_line(position, pV, Color.green)
	draw_line(position, -pV, Color.red)
	draw_string(font, position + Vector2(51,32), "v: " + String(pV), Color.green)
	
	# Position - Global Position Display
	draw_line(position, -global_position, Color.yellow)
	draw_string(font, position + Vector2(28,48), "gpos: " + String(global_position-position), Color.yellow)
	
	# Position - Mouse Display
	var l_globalMousePos = get_global_mouse_position() - global_position
	draw_circle(get_local_mouse_position(), 4.0, Color.pink)
	#draw_circle(l_globalMousePos, 4.0, Color.magenta)
	draw_string(font, get_local_mouse_position() + Vector2(5,4), "mouse", Color.pink)
	draw_line(position, get_local_mouse_position(), Color.pink) #Local
	draw_line(-global_position, l_globalMousePos, Color.magenta) #Global
	draw_line(get_local_mouse_position(), l_globalMousePos, Color.magenta) #Global
	
	# Direction v Velocity Display
	var nV = pV + get_local_mouse_position()
	nV = nV.normalized() * pV.length()
	#draw_line(position, nV, Color.cornsilk)
	
	# Face-forward
	#draw_line(position, Vector2(cos(pR),sin(pR)) * 100.0, Color.white)
	draw_line(position, Vector2(cos(pR),sin(pR)) * 10000.0, Color.white)
	draw_line(position, Vector2(cos(pR),sin(pR)) * -10000.0, Color.white)

func draw_circle_arc(center, radius, angle_from, angle_to, color):
	var nb_points = 32
	var points_arc = PoolVector2Array()

	for i in range(nb_points + 1):
		var angle_point = deg2rad(angle_from + i * (angle_to-angle_from) / nb_points - 90)
		points_arc.push_back(center + Vector2(cos(angle_point), sin(angle_point)) * radius)

	for index_point in range(nb_points):
		draw_line(points_arc[index_point], points_arc[index_point + 1], color)
