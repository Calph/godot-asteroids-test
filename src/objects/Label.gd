extends Label

var format = "Velocity X: %f\nVelocity Y: %f"

func _ready():
	update()
	pass

func update():
	text = format % [get_node("/root/Master/Player").velocity.x,get_node("/root/Master/Player").velocity.y]
	pass
